package com.absenta.movies.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.absenta.movies.R;
import com.absenta.movies.activity.presenter.MainActivityPresenter;
import com.absenta.movies.activity.presenter.impl.MainActivityPresenterImpl;
import com.absenta.movies.adapter.EndlessScrollListener;
import com.absenta.movies.adapter.MoviesAdapter;
import com.absenta.movies.adapter.RecyclerViewOnItemClickListener;
import com.absenta.movies.model.Movie;

import java.util.List;



public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private ProgressDialog progressDialog;
    // TODO - insert your themoviedb.org API KEY here
    public SwipeRefreshLayout swipeRefreshLayout;
    public RecyclerView recyclerView;
    public EndlessScrollListener scrollListener;
    public MainActivityPresenter mainActivityPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*if (API_KEY.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please obtain your API KEY from themoviedb.org first!", Toast.LENGTH_LONG).show();
            return;
        }*/
        mainActivityPresenter = new MainActivityPresenterImpl(this);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView = (RecyclerView) findViewById(R.id.movies_recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        scrollListener = new EndlessScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                mainActivityPresenter.onLoadMore(page);
            }
        };

        recyclerView.addOnScrollListener(scrollListener);

        recyclerView.setAdapter(new MoviesAdapter(mainActivityPresenter.getMoviesAcum(), new RecyclerViewOnItemClickListener() {
            @Override
            public void onClick(View v, int position) {
                //se hace el click del objecto en la lista
                Movie movie = mainActivityPresenter.getMoviesAcum().get(position);
                // Toast.makeText(v.getContext(), "movie id: " + movie.id + " " + movie.title, Toast.LENGTH_SHORT).show();
                mainActivityPresenter.goToDetail(movie.id);
            }
        }));

        mainActivityPresenter.loadList(1);

    }

    public void show() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setCancelable(false);
        }
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void hide() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onRefresh() {
        mainActivityPresenter.loadList(1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        // See above
        MenuItemCompat.setOnActionExpandListener(searchItem, new SearchViewExpandListener(this));
        MenuItemCompat.setActionView(searchItem, searchView);

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {

            @Override
            public boolean onClose() {
                Log.d(TAG, "onClose");
                mainActivityPresenter.getMoviesAcum().clear();
                mainActivityPresenter.loadList(1);
                return false;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                mainActivityPresenter.searchMovie(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        return true;
    }

    public void refreshAllList() {
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                recyclerView.getAdapter().notifyDataSetChanged();
            }
        });
    }

    public void showDetailAlert(Movie movie) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(movie.title)
                .setMessage("Details: " + movie.overview + " " + movie.originalLanguage + " origin Title: " + movie.originalTitle)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                    }
                })

                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void refreshListScroll(final List<Movie> moviesAcum) {
        final int curSize = recyclerView.getAdapter().getItemCount();
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                recyclerView.getAdapter().notifyItemRangeInserted(curSize, moviesAcum.size() - 1);
            }
        });
    }

    // See above
    private class SearchViewExpandListener implements MenuItemCompat.OnActionExpandListener {

        private Context context;

        public SearchViewExpandListener(Context c) {
            context = c;
        }

        @Override
        public boolean onMenuItemActionExpand(MenuItem item) {
            ((AppCompatActivity) context).getSupportActionBar().setDisplayShowHomeEnabled(true);
            return false;
        }

        @Override
        public boolean onMenuItemActionCollapse(MenuItem item) {
            ((AppCompatActivity) context).getSupportActionBar().setDisplayShowHomeEnabled(false);
            return false;
        }
    }

}

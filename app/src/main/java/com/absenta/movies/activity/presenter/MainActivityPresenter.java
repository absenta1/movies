package com.absenta.movies.activity.presenter;

import java.util.List;

import com.absenta.movies.model.Movie;

/**
 * Created by jmartinb on 18/10/2017.
 */

public interface MainActivityPresenter {

    void searchMovie(String query);

    void loadList(int page);

    void goToDetail(int movieId);

    List<Movie> getMoviesAcum();

    int getTotalPages();

    void onLoadMore(int page);
}

package com.absenta.movies.activity.presenter.impl;

import java.util.ArrayList;
import java.util.List;

import com.absenta.movies.activity.MainActivity;
import com.absenta.movies.activity.presenter.MainActivityPresenter;
import com.absenta.movies.model.Movie;
import com.absenta.movies.model.MoviesResponse;
import com.absenta.movies.rest.ApiClient;
import com.absenta.movies.rest.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jmartinb on 18/10/2017.
 */

public class MainActivityPresenterImpl implements MainActivityPresenter {

    private MainActivity mainActivity;
    private final static String API_KEY = "7e8f60e325cd06e164799af1e317d7a7";
    private List<Movie> moviesAcum = new ArrayList<>();
    private int totalPages = -1;
    private ApiInterface apiService;

    public MainActivityPresenterImpl(MainActivity mainActivity){
        this.mainActivity = mainActivity;
        apiService = ApiClient.getClient().create(ApiInterface.class);

    }

    @Override
    public void searchMovie(String query) {
        mainActivity.show();
        Call<MoviesResponse> call = apiService.searchMovie(query, API_KEY);
        call.enqueue(new Callback<MoviesResponse>() {
            @Override
            public void onResponse(Call<MoviesResponse> call, Response<MoviesResponse> response) {
                // String detail = response.body().toString();
                moviesAcum.clear();
                final List<Movie> movies = response.body().results;
                totalPages = response.body().totalPages;
                moviesAcum.addAll(movies);
                mainActivity.refreshAllList();
                mainActivity.hide();

            }

            @Override
            public void onFailure(Call<MoviesResponse> call, Throwable t) {
                // Log error here since request failed
                mainActivity.hide();
            }
        });
    }

    @Override
    public void loadList(int page) {
        mainActivity.show();
        Call<MoviesResponse> call = apiService.getTopRatedMovies(API_KEY, page);
        call.enqueue(new Callback<MoviesResponse>() {
            @Override
            public void onResponse(Call<MoviesResponse> call, Response<MoviesResponse> response) {
                int statusCode = response.code();
                final List<Movie> movies = response.body().results;
                totalPages = response.body().totalPages;
                moviesAcum.addAll(movies);
                mainActivity.refreshListScroll(moviesAcum);
                mainActivity.hide();
            }

            @Override
            public void onFailure(Call<MoviesResponse> call, Throwable t) {
                // Log error here since request failed
                mainActivity.hide();
            }
        });
    }

    @Override
    public void goToDetail(int movieId) {
        mainActivity.show();
        Call<Movie> call = apiService.getMovieDetails(movieId, API_KEY);
        call.enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
                // String detail = response.body().toString();
                Movie movie = response.body();
                mainActivity.showDetailAlert(movie);
                mainActivity.hide();

            }

            @Override
            public void onFailure(Call<Movie> call, Throwable t) {
                // Log error here since request failed
                mainActivity.hide();
            }
        });
    }

    public List<Movie> getMoviesAcum() {
        return moviesAcum;
    }

    public int getTotalPages() {
        return totalPages;
    }

    @Override
    public void onLoadMore(int page) {
        page = page + 1;
        if(page <= totalPages) {
            loadList(page);
        }
    }
}

package com.absenta.movies.adapter;

/**
 * Created by jmartinb on 18/10/2017.
 */

import android.view.View;

public interface  RecyclerViewOnItemClickListener {

    void onClick(View v, int position);
}
package com.absenta.movies.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import com.absenta.movies.R;
import com.absenta.movies.model.Movie;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MovieViewHolder>  {

    private List<Movie> movies;
    private RecyclerViewOnItemClickListener recyclerViewOnItemClickListener;

    public class MovieViewHolder extends RecyclerView.ViewHolder implements View
            .OnClickListener {
        LinearLayout moviesLayout;
        TextView movieTitle;
        TextView data;
        TextView movieDescription;
        TextView rating;


        public MovieViewHolder(View itemView) {
            super(itemView);
            moviesLayout = (LinearLayout) itemView.findViewById(R.id.movies_layout);
            movieTitle = (TextView) itemView.findViewById(R.id.title);
            data = (TextView) itemView.findViewById(R.id.subtitle);
            movieDescription = (TextView) itemView.findViewById(R.id.description);
            rating = (TextView) itemView.findViewById(R.id.rating);
            itemView.setOnClickListener(this);
        }

        public void onClick(View view) {
            recyclerViewOnItemClickListener.onClick(view,getAdapterPosition());
        }

    }

    public MoviesAdapter(List<Movie> movies,RecyclerViewOnItemClickListener recyclerViewOnItemClickListener) {
        this.movies = movies;
        this.recyclerViewOnItemClickListener = recyclerViewOnItemClickListener;
    }

    @Override
    public MoviesAdapter.MovieViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_movie, parent, false);
        return new MovieViewHolder(view);
    }


    @Override
    public void onBindViewHolder(MovieViewHolder holder, final int position) {
        holder.movieTitle.setText(movies.get(position).title);
        holder.data.setText(movies.get(position).releaseDate);
        holder.movieDescription.setText(movies.get(position).overview);
        holder.rating.setText(movies.get(position).voteAverage.toString());
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

}